# Usage

cd stable/alpine
docker build -f Dockerfile -t ngxwpx:latest .



# Run
cd envconf

// for prod
docker run -d --name my-ngx-proxy -v `pwd`/prod:/etc/nginx/conf.d -p 81:80 ngxwpx:latest

// for stage
docker run -d --name my-ngx-proxy -v `pwd`/stage:/etc/nginx/conf.d -p 81:80 ngxwpx:latest


# About this Repo base

This is the Git repo of the official Docker image for [nginx](https://registry.hub.docker.com/_/nginx/). See the
Hub page for the full readme on how to use the Docker image and for information
regarding contributing and issues.

The full readme is generated over in [docker-library/docs](https://github.com/docker-library/docs),
specificially in [docker-library/docs/nginx](https://github.com/docker-library/docs/tree/master/nginx).


#!/bin/bash

# --- START UserData SECTION ---------
#export ENV_REPO=git@bitbucket.org:hkd1123/docker-ngxwpx.git
#export ENV_TARGET_WP=https://hoge.hoge.com

# dev, stage, prod
#export ENV_MODE=prod

# ecqbase, ecqscale, wppx
#export ENV_TYPE=wppx

#export ENV_PRODBRANCH=prod
#export ENV_STAGEBRANCH=stage
#export ENV_DEVBRANCH=dev001

#export ENV_WORKBRANCH=prod

#sshcfg=$(cat <<SSHC
#Host bitbucket.org
#   HostName bitbucket.org
#   IdentityFile ~/.ssh/id_rsa_bb
#   StrictHostKeyChecking no
#SSHC
#)
#echo "$sshcfg" > /root/.ssh/config
#chmod 600 /root/.ssh/config

#sshcfg=$(cat <<SSHC
#(rsa key string)
#SSHC
#)
#echo "$sshcfg" > /root/.ssh/id_rsa_bb
# chown ec2-user:ec2-user /home/ec2-user/.ssh/id_rsa_bb
#chmod 600 /root/.ssh/id_rsa_bb

#cd /home/ec2-user
#git clone $ENV_REPO Wppx

#bash /home/ec2-user/Wppx/scripts/setup.sh

# --- END UserData SECTION ---------

cd /home/ec2-user/Wppx
envcfg=$(cat <<ENVC
export ENV_REPO=$ENV_REPO

export ENV_MODE=$ENV_MODE
export ENV_TYPE=$ENV_TYPE

export ENV_PRODBRANCH=$ENV_PRODBRANCH
export ENV_STAGEBRANCH=$ENV_STAGEBRANCH
export ENV_DEVBRANCH=$ENV_DEVBRANCH

export ENV_WORKBRANCH=$ENV_WORKBRANCH

ENVC
)
echo "$envcfg" > /home/ec2-user/envcfg

git checkout $ENV_WORKBRANCH

git config --add user.name "ecqdeploy"

ngxcfg=$(cat <<DBC
server {
    listen       80;
    server_name  localhost;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

    #location / {
    #    root   /usr/share/nginx/html;
    #    index  index.html index.htm;
    #}

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80
    #
    #location ~ \.php$ {
    #    proxy_pass   http://127.0.0.1;
    #}

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny  all;
    #}

    location ^~ / {
      proxy_pass    $ENV_TARGET_WP;
    }    
}
DBC
)
mkdir -p /home/ec2-user/nginx/conf.d


if [ $ENV_MODE = 'dev' ] ; then
    echo "$ngxcfg" > /home/ec2-user/nginx/conf.d/default.conf
elif [ $ENV_MODE = 'stage' ] ; then
    echo "$ngxcfg" > /home/ec2-user/nginx/conf.d/default.conf
elif [ $ENV_MODE = 'prod' ] ; then
    echo "$ngxcfg" > /home/ec2-user/nginx/conf.d/default.conf
else
    echo 'Enviroment mode is invalid.'
    exit 1
fi

cd /home/ec2-user

if [ $ENV_TYPE = 'wppx' ] ; then
# wp reverse proxy
docker pull registry.mobingi.com/accdesign/ngxwpx:latest

docker run -d --restart=always --name my-ngxwpx -v `pwd`/nginx/conf.d:/etc/nginx/conf.d -p 81:80 registry.mobingi.com/accdesign/ngxwpx:latest
    exit 0
fi
exit 0